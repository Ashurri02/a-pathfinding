#include "windowManager.h"

void windowManager::windowInitalise()
{
	std::ifstream inputStream;
	inputStream.open("Resources/window.ini");

	//Set defaults
	std::string appname = "A* PathFinding Algorithmn";
	sf::VideoMode windowBounds(1920, 1080);
	unsigned frameRateLimit = 60;
	bool isVerticalSyncEnabled = false;

	if (inputStream.is_open())
	{
		std::getline(inputStream, appname);
		inputStream >> windowBounds.width >> windowBounds.height;
		inputStream >> frameRateLimit;
		inputStream >> isVerticalSyncEnabled;	
	}
	this->appWindow = new sf::RenderWindow(windowBounds, appname);
	this->appWindow->setFramerateLimit(frameRateLimit);
	this->appWindow->setVerticalSyncEnabled(isVerticalSyncEnabled);
}

windowManager::windowManager()
{
	this->windowInitalise();
}

windowManager::~windowManager()
{
	delete this->appWindow;
}



