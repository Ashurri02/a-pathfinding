#include <iostream>
#include <SFML/Graphics.hpp>

#include "Window.h"

Window programWindow;

int main()
{
	while (programWindow.getWindowIsOpten())
	{
		programWindow.update();

		programWindow.render();
	}
	return 0;
}