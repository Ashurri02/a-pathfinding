#pragma once
#ifndef WINDOWMANAGER_H
#define WINDOWMANAGER_H

//Library Includes
#include <iostream>
#include <fstream>
#include <sstream>
//SFML Includes 
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

//Local Includes

/*
	Class Declaration
	------------------
	this Class is responsible for updating the window and delta time.
*/

class windowManager
{
private:
	sf::RenderWindow *appWindow;
	sf::Event newEvent;
	sf::Clock deltaTimeClock;

	float deltaTime;

	//initialise
	void windowInitalise();

public:

	windowManager();
	~windowManager();

	//Fucntions
	void updateDeltaTime();
	void updateSFMLevents();
	void Update();
	void Run();
	void Render();


};

#endif

