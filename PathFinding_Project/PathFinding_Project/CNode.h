#pragma once


#ifndef _CNODE_H__
#define _CNODE_H__

class CNode {
private:
	int id;
	int globalGoal;
	int localGoal;
	CNode* parent;
	CNode* connectionUp;
	CNode* connectionDown;
	CNode* connectionRight;
	CNode* connectionLeft;
public:
	CNode();
	int getID();
	void setID(int newID);
	int getGlobal();
	void setGlobal(int newGlobal);
	int getLocal();
	void setLocal(int newLocal);
	CNode* getParent();
	void setParent(CNode* newParent);
	CNode* getConnection(int direction);
	void setConnection(int direction, CNode* newConnection);
};

#endif