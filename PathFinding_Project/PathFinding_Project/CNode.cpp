#include "CNode.h"

CNode::CNode() //Initializes CNode variables
{
	id = 0;
	globalGoal = 1000000000;
	localGoal = 1000000000;
	parent = nullptr;
	connectionUp = nullptr;
	connectionDown = nullptr;
	connectionRight = nullptr;
	connectionLeft = nullptr;
}

int CNode::getID() //Gets ID of node
{
	return (id);
}

void CNode::setID(int newID) //Sets ID of node
{
	id = newID;
}

int CNode::getGlobal() //Gets global goal of node
{
	return (globalGoal);
}

void CNode::setGlobal(int newGlobal) //Sets global goal of node
{
	globalGoal = newGlobal;
}

int CNode::getLocal() //Gets local goal of node
{
	return (localGoal);
}

void CNode::setLocal(int newLocal) //Sets local goal of node
{
	localGoal = newLocal;
}

CNode* CNode::getParent() //Gets parent of node
{
	return (parent);
}

void CNode::setParent(CNode* newParent) //Sets parent of node
{
	parent = newParent;
}

CNode* CNode::getConnection(int direction) //Gets connection of node in specified direction
{
	switch (direction)
	{
	case 0:
	{
		return (connectionUp);
	}
	case 1:
	{
		return (connectionDown);
	}
	case 2:
	{
		return (connectionRight);
	}
	case 3:
	{
		return (connectionLeft);
	}
	}
}

void CNode::setConnection(int direction, CNode* newConnection) //Sets connection of node in specified direction
{
	switch (direction)
	{
	case 0:
	{
		connectionUp = newConnection;
	}
	case 1:
	{
		connectionDown = newConnection;
	}
	case 2:
	{
		connectionRight = newConnection;
	}
	case 3:
	{
		connectionLeft = newConnection;
	}
	}
}
