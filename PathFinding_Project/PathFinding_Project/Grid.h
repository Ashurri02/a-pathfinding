#include <SFML/Graphics.hpp>
#include <list>
#pragma once

class Grid
{
public:
	Grid();
	~Grid();
	enum TileState
	{
		Start,
		End,
		Barrier,
		Empty,
		Path
	};

	struct sNode {

		TileState state;
		float fGlobalCost, fLocalCost;
		int xPos, YPos;
		bool hasVisted;
		bool isObstacle;
		std::vector<sNode*> Neighbors;
		sNode* parent;
		sf::RectangleShape* sf_Square;
	};
	sNode *nodes;
	sf::RectangleShape getSquare(int xIndex, int yIndex)const;


	void setStartSquare(int xIndex, int yIndex);

	void setEndSquare(int xIndex, int yIndex);

	void setBarrierSquare(int xIndex, int yIndex);

	void setEmptySquare(int xIndex, int yIndex);

	void setPathSquare();
	
	sNode* Startnode = new sNode();
	sNode* Endnode = new sNode();
	sNode* tempPointer;
	

	void solve_Astar();
private:
	std::list<Grid::sNode*> OpenList;


	int StartXIndex;
	int StartYIndex;

	int EndXIndex;
	int EndYIndex;
};
