#include <SFML/Graphics.hpp>
#include <vector>
#include <iostream>
#include "Grid.h"

#pragma once

class Window
{
public:
	Window();
	~Window();

	void poolEvents();
	void update();
	void render();

	bool getWindowIsOpten()const;

	
private:
	//variables
	sf::RenderWindow* programWindow;
	sf::Event evnt;

	//cursor
	sf::RectangleShape* cursor;

	Grid Matrix;
	int BlockSelected;

	
	enum BlockChoice
	{
		Start,
		End,
		Barrier,
		Empty
	};
};

