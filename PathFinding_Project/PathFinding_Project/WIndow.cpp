#include "Window.h"

Window::Window()
{
	//setting window and limiting to 60fps.
	programWindow = new sf::RenderWindow(sf::VideoMode(1280, 720), "A Star Path Finding", sf::Style::Titlebar | sf::Style::Close);
	programWindow->setVerticalSyncEnabled(true);
	programWindow->setFramerateLimit(60);

	//setting cursor size and color
	cursor = new sf::RectangleShape(sf::Vector2f(1, 1));
	cursor->setFillColor(sf::Color::White);

	BlockSelected = Start;

	
}

Window::~Window()
{
	delete programWindow;
}

void Window::poolEvents()
{
	while (this->programWindow->pollEvent(evnt))
	{
		switch (evnt.type)
		{
		case sf::Event::Closed:
			this->programWindow->close();
			break;
		default:
			break;
		}
	}

	//Inputs 
		//drawing each tile in matrix
	for (int i = 0; i < 20; i++)
	{
		for (int j = 0; j < 20; j++)
		{
			if(sf::Mouse::isButtonPressed(sf::Mouse::Left) && (cursor->getGlobalBounds().intersects(Matrix.getSquare(i, j).getGlobalBounds())))
			{

				switch (BlockSelected)
				{
				case Start:
				Matrix.setStartSquare(i, j);
					break;

				case End:
					Matrix.setEndSquare(i, j);
					break;
				case Barrier:
					Matrix.setBarrierSquare(i, j);
					break;
				case Empty:
					Matrix.setEmptySquare(i, j);
					break;
				default:
					break;
				}
				
			}
		}
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num1))
	{
		BlockSelected = Start;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num2))
	{
		BlockSelected = End;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num3))
	{
		BlockSelected = Barrier;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num4))
	{
		BlockSelected = Empty;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Enter))
	{
		Matrix.solve_Astar();
		Matrix.setPathSquare();
		std::cout << "runAstar" << std::endl;
	}

}

void Window::update()
{
	//this-> ?
	poolEvents();

	cursor->setPosition(sf::Mouse::getPosition(*programWindow).x, sf::Mouse::getPosition(*programWindow).y);

	//update
}

void Window::render()
{
	programWindow->clear(sf::Color::Black);

	//draw objects
	programWindow->draw(*cursor);

	//drawing each tile in matrix
	for (int i = 0; i < 20; i++)
	{
		for (int j = 0; j < 20; j++)
		{
			programWindow->draw(Matrix.getSquare(i, j));
		}
	}

	programWindow->display();
}

bool Window::getWindowIsOpten() const
{
	return programWindow->isOpen();
}
