#include "Grid.h"
#include <iostream>
Grid::Grid()
{
	nodes = new sNode[20*20];
	//setting each square in the grid
	for (int i = 0; i < 20; i++)
	{
		for (int j = 0; j < 20; j++)
		{

			nodes[j * 20 + i].sf_Square = new sf::RectangleShape(sf::Vector2f(30.0f, 30.0f));
			nodes[j * 20 + i].sf_Square->setOutlineColor(sf::Color::White);
			nodes[j * 20 + i].sf_Square->setPosition((i * 40), (j * 36));
			//SquareGrid[i][j] = Empty;
			nodes[j * 20 + i].state = TileState::Empty;
			nodes[j * 20 + i].xPos = i;
			nodes[j * 20 + i].YPos = j;
			nodes[j * 20 + i].parent = nullptr;
			nodes[j * 20 + i].isObstacle = false;
			nodes[j * 20 + i].hasVisted = false;

			if (j > 0)
			{
				nodes[j * 20 + i].Neighbors.push_back(&nodes[(j - 1) * 20 + (i + 0)]);
			}
			if (j < 20 - 1)
			{
				nodes[j * 20 + i].Neighbors.push_back(&nodes[(j + 1) * 20 + (i + 0)]);
			}

			if (i > 0)
			{
				nodes[j * 20 + i].Neighbors.push_back(&nodes[(j + 0) * 20 + (i - 1)]);
			}
			if (i < 20 - 1)
			{
				nodes[j * 20 + i].Neighbors.push_back(&nodes[(j + 0) * 20 + (i + 1)]);
			}
			
		}
	}
	tempPointer = Endnode;

}

Grid::~Grid()
{
	delete nodes;
}

sf::RectangleShape Grid::getSquare(int xIndex, int yIndex) const
{
		return *nodes[yIndex * 20 + xIndex].sf_Square;
}

void Grid::setStartSquare(int xIndex, int yIndex)
{
	std::cout << "start square Placed" << std::endl;
	
	//set the previous square back to normal
	nodes[StartYIndex * 20 + StartXIndex].sf_Square->setFillColor(sf::Color::White);
	nodes[StartYIndex * 20 + StartXIndex].state = TileState::Empty;
	//set the selected square to the start position
	nodes[yIndex * 20 + xIndex].sf_Square->setFillColor(sf::Color::Blue);
	nodes[yIndex * 20 + xIndex].state = TileState::Start;
	StartXIndex = xIndex;
	StartYIndex = yIndex;
	*Startnode = nodes[yIndex * 20 + xIndex];
}

void Grid::setEndSquare(int xIndex, int yIndex)
{
	std::cout << "end square Placed" << std::endl;
	
	//set the previous square back to normal
	nodes[EndYIndex * 20 + EndXIndex].sf_Square->setFillColor(sf::Color::White);
	nodes[EndYIndex * 20 + EndXIndex].state = TileState::Empty;
	//set the selected square to the start position
	nodes[yIndex * 20 + xIndex].sf_Square->setFillColor(sf::Color::Green);
	nodes[yIndex * 20 + xIndex].state = TileState::End;
	EndXIndex = xIndex;
	EndYIndex = yIndex;
	Endnode = &nodes[yIndex * 20 + xIndex];
}

void Grid::setBarrierSquare(int xIndex, int yIndex)
{
	//set the selected square to a barrier tile
	nodes[yIndex * 20 + xIndex].sf_Square->setFillColor(sf::Color::Red);
	nodes[yIndex * 20 + xIndex].state = Barrier;
}

void Grid::setEmptySquare(int xIndex, int yIndex)
{
	//set the selected square to an empty tile
	nodes[yIndex * 20 + xIndex].sf_Square->setFillColor(sf::Color::White);
	nodes[yIndex * 20 + xIndex].state = Empty;
}

void Grid::setPathSquare()
{
	if (Endnode->parent != nullptr)
	{
		tempPointer = Endnode->parent;
		while (tempPointer->parent != nullptr)
		{			
			nodes[tempPointer->YPos * 20 + tempPointer->xPos].sf_Square->setFillColor(sf::Color::Cyan);
			nodes[tempPointer->YPos * 20 + tempPointer->xPos].state = Path;

			tempPointer = tempPointer->parent;

		}
		
	}
	//set the selected square to a Path tile
	
}

void Grid::solve_Astar()
{
	if (Startnode != nullptr && Endnode != nullptr)
	{
		for (int i = 0; i < 20; i++)
		{
			for (int j = 0; j < 20; j++)
			{
				nodes[j * 20 + i].fGlobalCost = INFINITY;
				nodes[j * 20 + i].fLocalCost = INFINITY;
				nodes[j * 20 + i].parent = nullptr;
				nodes[j * 20 + i].hasVisted = false;
				//if (nodes[j * 20 + i].state == TileState::Path)
				//{
				//	nodes[j * 20 + i].sf_Square->setFillColor(sf::Color::White);
				//	nodes[j * 20 + i].state = Empty;
				//}


			}
		}
		auto Distance = [](sNode* _a, sNode* _b)
		{
			return sqrtf((_a->xPos - _b->xPos) * (_a->xPos - _b->xPos) + (_a->YPos - _b->YPos) * (_a->YPos - _b->YPos));
		};

		auto Heuristic = [Distance](sNode* _a, sNode* _b)
		{
			return Distance(_a, _b);
		};

		//Start
		sNode* currentNode = Startnode;
		Startnode->fLocalCost = 0.0f;
		Startnode->fGlobalCost = Heuristic(Startnode, Endnode);

		OpenList.push_back(Startnode);

		while (!OpenList.empty())
		{

			OpenList.sort([](const sNode* lhs, const sNode* rhs) {return lhs->fGlobalCost < rhs->fGlobalCost; });
			while (!OpenList.empty() && OpenList.front()->hasVisted)
			{
				OpenList.pop_front();
			}
			if (OpenList.empty())
			{
				break;
			}

			currentNode = OpenList.front();
			currentNode->hasVisted = true;

			for (int i = 0; i < currentNode->Neighbors.size(); i++) //sNode* Neighbor in currentNode->Neighbors)
			{
				//sNode* Neighbor = currentNode->Neighbors.at(i);
				if (!currentNode->Neighbors.at(i)->hasVisted && currentNode->Neighbors.at(i)->state != TileState::Barrier)
				{
					OpenList.push_back(currentNode->Neighbors.at(i));

					float fPossiblelowerGoal = currentNode->fLocalCost + Distance(currentNode, currentNode->Neighbors.at(i));
					if (fPossiblelowerGoal < currentNode->Neighbors.at(i)->fLocalCost)
					{
						if (currentNode->Neighbors.at(i)->state == TileState::End)
						{
							std::cout << "Test";
						}
						currentNode->Neighbors.at(i)->parent = currentNode;
						currentNode->Neighbors.at(i)->fLocalCost = fPossiblelowerGoal;

						currentNode->Neighbors.at(i)->fGlobalCost = currentNode->Neighbors.at(i)->fLocalCost + Heuristic(currentNode->Neighbors.at(i), Endnode);
					}
				}

			}

		}

	}
	else
	{

		std::cout << "Start Node or EndNode is not placed" << std::endl;
	}
	
}
